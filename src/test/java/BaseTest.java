import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {

    @BeforeScenario
    public void hazirlik (){
        System.out.println("Senaryo Basliyor");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        wait = new WebDriverWait(driver, 40);
        driver.manage().window().maximize();

    }

    static WebDriver driver;
    static Actions action;
    static WebDriverWait wait;

    public static void getUrl(){
        driver.get("https://www.yapikredi.com.tr/");
    }

    public WebElement findElement(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }

    public void clickElement(String by){
        findElement(by).click();
    }

    public void openNewTab(){
        for(String childWindow: driver.getWindowHandles())
        {
            driver.switchTo().window(childWindow);
        }
    }

    public void hooverElement(String by){
        action.moveToElement(findElement(by)).build().perform();
    }

    public void sendKeyElements(String by, String text){
        findElement(by).sendKeys(text);
    }

    public void waitUntilExist(String by){
        wait.until(ExpectedConditions.elementToBeClickable(findElement(by)));
    }

    @AfterScenario
    public void bitir() {
        driver.quit();
        System.out.println("Senaryo Tamamlandi");
    }

}