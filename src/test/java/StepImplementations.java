import com.thoughtworks.gauge.Step;
import org.junit.Assert;

public class StepImplementations extends BaseTest {
    @Step("Yapikredi ana sayfasina git")
    public void mainPage() {
        getUrl();
    }

    @Step("<Element> butonuna tikla")
    public void clickButton(String element) {
        clickElement(element);
        System.out.println(element + " butonuna tiklandi");
    }

    @Step("<saniye> sn. bekle")
    public void waitSecond(int key) throws InterruptedException {
        Thread.sleep(key * 1000);
        System.out.println(key + " sn. beklendi.");
    }

    @Step("Yeni pencereye gec")
    public void newTab() {
        openNewTab();
        System.out.println("Yeni pencerede devam edildi");
    }

    @Step("<input> a <inputValue> gir")
    public void fillInput(String input, String key) {
        sendKeyElements(input, key);
    }

    @Step("<Element> kontrol et")
    public void checkStep(String element) {
        try {
            findElement(element);
            System.out.println(element + " kontrol edildi.");
        } catch (Exception e) {
            Assert.fail("Element bulunamadi.");
        }
    }

    @Step("<element> yuklenmesini bekle")
    public void waitPage(String element) {
        waitUntilExist(element);
    }
}
